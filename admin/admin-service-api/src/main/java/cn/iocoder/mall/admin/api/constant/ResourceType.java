package cn.iocoder.mall.admin.api.constant;

/**
 * 资源类型
 */
public interface ResourceType {

    /**
     * 彩蛋
     */
    Integer MENU = 1;
    /**
     * URL
     */
    Integer URL = 2;

}
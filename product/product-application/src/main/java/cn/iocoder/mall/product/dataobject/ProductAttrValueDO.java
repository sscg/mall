package cn.iocoder.mall.product.dataobject;

import java.util.Date;

/**
 * Product 规格值
 */
public class ProductAttrValueDO {

    /**
     * 规格值编号
     */
    private Integer id;
    /**
     * 规格编号
     */
    private Integer attrId;
    /**
     * 规格值
     */
    private String name;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后更新时间
     */
    private Date updateTime;
    /**
     * 状态
     *
     * 1-正常
     * 2-删除
     */
    private Integer status;

}
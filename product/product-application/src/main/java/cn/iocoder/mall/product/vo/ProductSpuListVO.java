package cn.iocoder.mall.product.vo;

import java.util.List;

public class ProductSpuListVO {

    /**
     * SPU 数组
     */
    private List<ProductSpuVO> list;
    /**
     * 是否还有下一页
     */
    private Boolean hasNext;

}